import { createWorker } from 'tesseract.js';

const worker = createWorker({
  logger: (m) => console.log(m),
});

(async () => {
  await worker.load();
  await worker.loadLanguage('kor+eng');
  await worker.initialize('kor+eng');
  const { data: { text } } = await worker.recognize('https://search.pstatic.net/common/?src=http%3A%2F%2Fblogfiles.naver.net%2FMjAyMTEwMTNfNzkg%2FMDAxNjM0MTEyMzk0Njc1.3BGddnd_wwIsPikwIWfHBR3k-SO3VtHi69yKM-LtWNwg.S-AdaTEfwBLWsX37dgzogOGzXr6WrjEYjd33yUv5Rjwg.JPEG.loveyum27%2F1634112391666.jpg&type=sc960_832');
  console.log(text);
  await worker.terminate();
})();
