"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const tesseract_js_1 = require("tesseract.js");
const worker = tesseract_js_1.createWorker({
    logger: (m) => console.log(m),
});
(() => __awaiter(void 0, void 0, void 0, function* () {
    yield worker.load();
    yield worker.loadLanguage('kor+eng');
    yield worker.initialize('kor+eng');
    const { data: { text } } = yield worker.recognize('https://search.pstatic.net/common/?src=http%3A%2F%2Fblogfiles.naver.net%2FMjAyMTEwMTNfNzkg%2FMDAxNjM0MTEyMzk0Njc1.3BGddnd_wwIsPikwIWfHBR3k-SO3VtHi69yKM-LtWNwg.S-AdaTEfwBLWsX37dgzogOGzXr6WrjEYjd33yUv5Rjwg.JPEG.loveyum27%2F1634112391666.jpg&type=sc960_832');
    console.log(text);
    yield worker.terminate();
}))();
//# sourceMappingURL=index.js.map